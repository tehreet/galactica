# Galactica
A software composition analysis tool for Python (other languages coming soon!).

## Getting Started

### Install Dependencies
The `requirements.txt` file contains the dependencies and versions used for the project.  Run the following command to 
install the dependencies:
```
> pip install -r requirements.txt
```

## Configuration

Edit settings.yml with the proper configuration values.  To leverage the ability to symmetrically encrypt any sensitive 
information, make sure to prefix the string values with `(enc)`.  Currently encryption is only supported at two levels 
of a configuration file.

## Usage

### Encrypt Settings

Run the script with the `--enc` option to encrypt any strings prefixed with `(enc)`.

```
> python main.py --enc
```

Once run, the script will create a hidden file with the key used to encrypt and decrypt values.  The values previously 
prefixed with `(enc)` will be changed to a base64 value now prefixed with `(dec)`.
 
### Running Script

Right now, this doesn't do anything but read it's own `requirements.txt`.

A local vulnerability json file is manually created for now. When the 
application is run, it reads a `requirements.txt` file and produces a JSON 
output for vulnerable dependencies, including the advisory and any applicable
CVEs.


No modifications are needed to enable logging or to load any encrypted settings.  The currently provided script will load
the encrypted settings and print them in cleartext to show they were properly loaded.

```
> python main.py
```

You can specify the configuration file location (eg /tmp/settings.yml) or leverage the default file found in 
`../conf/settings.yml`.

