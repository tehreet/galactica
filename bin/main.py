import sys
sys.path.append('../')

from lib.utilities import logger_utility
from lib.utilities import config_utility
from lib.helpers import read_requirements

import argparse
import logging
import os
from ruamel.yaml import YAML

import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))


def main():
    # Handle basic arguments
    parser = argparse.ArgumentParser(description='A cross-platform software composition analysis tool')
    parser.add_argument('--enc', dest='enc', action='store_true', help='encrypt configuration file')
    parser.add_argument('--config', dest='config', type=str, default='../conf/settings.yml',
                        help='location of settings file')
    args = parser.parse_args()

    # Load configuration file
    yaml = YAML()
    settings = yaml.load(open(args.config))

    # Get encryption key
    key = config_utility.key(settings['encryption_key']) if 'encryption_key' in settings else None

    # Encrypt configuration
    if args.enc and key is not None:
        enc_config = config_utility.encrypt(settings, key)

        config_utility.save_config(enc_config, args.config)

    settings = config_utility.decrypt(settings, key)

    # Configure logger
    logger_utility.setup_logging(settings['logger_config'])
    # Instantiate for main
    logger = logging.getLogger(__name__)
    gr = read_requirements.GatherRequirements(settings)
    if settings['use_example'] is True:
        requirements = open("../example/requirements.txt", 'r')
    else:
        requirements = open("requirements file here", 'r')
    gr.gather_requirements(requirements)


if __name__ == "__main__":
    main()
