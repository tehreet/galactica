from dparse.parser import setuptools_parse_requirements_backport as _parse_requirements
from lib.utilities import logger_utility
import logging
import semver
import os
import json

def iter_lines(fh, lineno=0):
    for line in fh.readlines()[lineno:]:
        yield line


def parse_line(line):
    strings = ('http://', 'e', 'https://')
    if line.startswith(strings):
        if "#egg=" in line:
            line = line.split("#egg=")[-1]
    return _parse_requirements(line)


class GatherRequirements:
    def __init__(self, settings):
        logger_utility.setup_logging(settings['logger_config'])
        self.logger = logging.getLogger(__name__)
        self.settings = settings

    def gather_requirements(self, requirements, resolve=True):
        kvps = {}
        strings = ('#', '-i', '--index-url', '--extra-index-url', '-f', '--no-index', '--find-links', '--allow-external',
                   '--allow-unverified', '-Z', '--always-unzip')
        is_temp_file = not hasattr(requirements, 'name')
        for num, line in enumerate(iter_lines(requirements)):
            line = line.strip()
            if not line:
                # skip empty lines
                continue
            if line.startswith(strings):
                continue
            elif line.startswith('-r') or line.startswith('--requirement'):
                # got a referenced file here, try to resolve the path
                # if this is a tempfile, skip
                if is_temp_file:
                    continue
                filename = line.strip("-r ").strip("--requirement").strip()
                # if there is a comment, remove it
                print(filename)
                if " #" in filename:
                    filename = filename.split(" #")[0].strip()
                req_file_path = os.path.join(os.path.dirname(requirements.name), filename)
                if resolve:
                    # recursively yield the resolved requirements
                    if os.path.exists(req_file_path):
                        with open(req_file_path) as _fh:
                            for req in _fh:
                                print(req)
                else:
                    print(req_file_path)
            else:
                parseable_line = line
                # multiline requiremdents are not parsable
                if "\\" in line:
                    parseable_line = line.replace("\\", "")
                    for next_line in iter_lines(requirements, num + 1):
                        parseable_line += next_line.strip().replace("\\", "")
                        line += "\n" + next_line
                        if "\\" in next_line:
                            continue
                        break
                req, = parse_line(parseable_line)
                if len(req.specifier._specs) == 1 and \
                        next(iter(req.specifier._specs))._spec[0] == "==":
                    version = next(iter(req.specifier._specs))._spec[1]
                    kvp = {req.name: {"version": version}}
                    kvps.update(kvp)
                else:
                    try:
                        fname = requirements.name
                    except AttributeError:
                        fname = line

                    print(f"Unpinned version of '{req.name}' found in {fname}, "
                          "unable to check this.")

        with open(self.settings['vulnerability_database'], 'r') as f:
            vuln_database = json.load(f)

        dependencies = {}
        cve_collection = {}
        for key, value in kvps.items():
            if key in vuln_database:
                cve_collection = {key: {"Version": value['version'], "Issues": []}}
                for x in vuln_database[key]:
                    if semver.match(value['version'], x['v']):
                        cve_collection[key]['Issues'].append({"Advisory": [x['advisory']], "CVE": [x['cve']]})
                dependencies.update(cve_collection)
            else:
                self.logger.info(f"{key} is not vulnerable!")
        print(json.dumps(dependencies, indent=4, sort_keys=False))
