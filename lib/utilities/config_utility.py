from cryptography.fernet import Fernet
import os.path
from ruamel.yaml import YAML
from ruamel.yaml.comments import CommentedMap, CommentedSeq


def key(filename):
    if os.path.isfile(filename):
        key_file = open(filename, 'r')
        key = key_file.readline()
    else:
        key = Fernet.generate_key()
        output = open(filename, 'wb')
        output.write(key)

    return Fernet(key)


def encrypt_string(string, key):
    if str(string).startswith('(enc)') == 1:
        enc_string = key.encrypt(str.encode(string.split('(enc)')[1]))
        return '(dec)' + enc_string.decode()
    else:
        return string


def encrypt(content, fernet_key):
    """
    Encrypt configuration file; Recursively looks for strings to encrypt

    :param content: The dictionary of the configuration file
    :param str fernet_key: The key used to encrypt the values
    :return: dictionary of encrypted settings
    """
    if type(content) is CommentedMap or type(content) is dict:
        for content_key, item in content.items():
            if type(item) is CommentedMap or type(item) is dict:
                content[content_key] = encrypt(item, fernet_key)
            elif type(item) is CommentedSeq or type(item) is list:
                for idx, i in enumerate(item):
                    content[content_key][idx] = encrypt(i, fernet_key)
            elif type(item) is str:
                content[content_key] = encrypt_string(item, fernet_key)
    elif type(content) is CommentedSeq or type(content) is list:
        for idx, item in enumerate(content):
            if type(item) is CommentedMap or type(item) is dict:
                content[idx] = encrypt(item, fernet_key)
            elif type(item) is CommentedSeq or type(item) is list:
                for idx2, i in enumerate(item):
                    content[idx][idx2] = encrypt(i, fernet_key)
            elif type(item) is str:
                content[idx] = encrypt_string(item, fernet_key)
    elif type(content) is str:
        content = encrypt_string(content, fernet_key)

    return content


def decrypt(content, fernet_key):
    """
    Decrypt configuration file; Recursively looks for strings to decrypt

    :param content: The dictionary of the configuration file with encrypted values
    :param str fernet_key: The key used to decrypt the values
    :return: dictionary of decrypted settings
    """
    if type(content) is CommentedMap or type(content) is dict:
        for content_key, item in content.items():
            if type(item) is CommentedMap or type(item) is dict:
                content[content_key] = decrypt(item, fernet_key)
            elif type(item) is CommentedSeq or type(item) is list:
                for idx, i in enumerate(item):
                    content[content_key][idx] = decrypt(i, fernet_key)
            elif type(item) is str:
                if item.startswith('(dec)'):
                    content[content_key] = fernet_key.decrypt(item.split('(dec)')[1].encode()).decode()
                else:
                    content[content_key] = item
    elif type(content) is CommentedSeq or type(content) is list:
        for idx, item in enumerate(content):
            if type(item) is CommentedMap or type(item) is dict:
                content[idx] = decrypt(item, fernet_key)
            elif type(item) is CommentedSeq or type(item) is list:
                for idx2, i in enumerate(item):
                    content[idx][idx2] = decrypt(i, fernet_key)
            elif type(item) is str:
                if item.startswith('(dec)'):
                    content[idx] = fernet_key.decrypt(item.split('(dec)')[1].encode()).decode()
                else:
                    content[idx] = item
    elif type(content) is str:
        if content.startswith('(dec)'):
            content = fernet_key.decrypt(content.split('(dec)')[1].encode()).decode()
        else:
            content = content

    return content


def save_config(content, file_location):
    yaml = YAML()
    yaml.default_flow_style = False

    with open(file_location, 'w') as outfile:
        yaml.dump(content, outfile)

